package com.example.listviewexample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

public class DetailsActivity extends AppCompatActivity {
    TextView txtID, txtPath, txtDesc;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        txtID = findViewById(R.id.idTextView);
        txtPath = findViewById(R.id.pathTextView);
        txtDesc = findViewById(R.id.descTextView);
        imageView = findViewById(R.id.img);
        Intent i=this.getIntent();
        String id=i.getExtras().getString("ID_KEY");
        String path=i.getExtras().getString("PATH_KEY");
        String desc=i.getExtras().getString("DESC_KEY");
        txtID.setText(id);
        txtDesc.setText(desc);
        txtPath.setText(path);
        Glide.with(getApplicationContext()).load(path).into(imageView);
    }
}