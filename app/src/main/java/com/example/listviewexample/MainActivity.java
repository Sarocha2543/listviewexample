package com.example.listviewexample;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {
    public static final String BASE_APP = "http://10.0.2.2/retrofit/";
    ListView listView;
    ListViewAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //defining a progress dialog to show while loading
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.show();
        // 1. Building retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ImageAPI.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        // 2. Defining retrofit api service
        ImageAPI spacecraftAPI = retrofit.create(ImageAPI.class);
        // 3. Defining the call
        Call<List<ImagesList>> call = spacecraftAPI.getImages();
        // 4. Calling the api as synchronous request
        //Create a retrofit2.Callback object and
        //override it’s onResponse and onFailure method.
        call.enqueue(new Callback<List<ImagesList>>() {
            @Override
            public void onResponse(Call<List<ImagesList>> call,
                                   Response<List<ImagesList>> response) {
                //hiding progress dialog
                progressDialog.dismiss();
                if (response.isSuccessful()) {
                    // 5. Converted JSON result stored in response.body()
                    // and put response data to images list
                    populateListView(response.body());
                }
            }
            @Override
            public void onFailure(Call<List<ImagesList>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(),
                        Toast.LENGTH_LONG).show();
                Log.e("retrofitreponse",t.getMessage());
            }
        });
    }
    private void populateListView(List<ImagesList> imagesList) {
        listView = findViewById(R.id.listViewImages);
        adapter = new ListViewAdapter(this,imagesList);
        listView.setAdapter(adapter);
    }
    class ListViewAdapter extends BaseAdapter {
        private List<ImagesList> imagesLists;
        private Context context;
        public ListViewAdapter(Context context, List<ImagesList> imagesLists) {
            this.context = context;
            this.imagesLists = imagesLists;
        }
        @Override
        public int getCount() {
            return imagesLists.size();
        }
        @Override
        public Object getItem(int pos) {
            return imagesLists.get(pos);
        }
        @Override
        public long getItemId(int pos) {
            return pos;
        }
        @Override
        public View getView(int position, View view, ViewGroup viewGroup) {
            if (view == null) {
                view = LayoutInflater.from(context).inflate(R.layout.listrow, viewGroup,
                        false);
            }
            TextView txtID = view.findViewById(R.id.idTextView);
            TextView txtPath = view.findViewById(R.id.pathTextView);
            TextView txtDesc = view.findViewById(R.id.descTextView);
            ImageView imageView = view.findViewById(R.id.img);
            final ImagesList thisimage = imagesLists.get(position);
            txtID.setText(thisimage.getId());
            txtDesc.setText(thisimage.getDescription());
            txtPath.setText(thisimage.getPath());
            if (thisimage.getPath() != null && thisimage.getPath().length() > 0) {
                Glide.with(context).load(BASE_APP+thisimage.getPath()).into(imageView);
            } else {
                Toast.makeText(context, "Empty Image URL", Toast.LENGTH_LONG).show();
                Glide.with(context).load(R.mipmap.ic_launcher_round).into(imageView);
            }
            view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(context, thisimage.getId(),
                            Toast.LENGTH_SHORT).show();
                    String[] images = {
                            thisimage.getId(),
                            BASE_APP+thisimage.getPath(),
                            thisimage.getDescription()
                    };
                    openDetailActivity(images);
                }
            });
            return view;
        }
        private void openDetailActivity(String[] data) {
            Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
            intent.putExtra("ID_KEY", data[0]);
            intent.putExtra("PATH_KEY", data[1]);
            intent.putExtra("DESC_KEY", data[2]);
            startActivity(intent);
        }
    }
}